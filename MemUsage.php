<?php
declare(strict_types=1);

trait MemUsage
{
    /**
     * @param int $start
     *
     * @return string
     */
    public static function viewMemUsage(int $start)
    {
        $mem = memory_get_peak_usage();
        return 'MEM MAX USAGE: '
            . ($mem > (1024)
                ? ($mem > 1048576 ? ($mem / 1024 / 1024) . ' Mb' : ($mem / 1024) . ' Kb')
                : $mem . ' byte')
            . '   Execution time: ' . ((time() - $start) / 60) ;
    }
}
