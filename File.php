<?php

require_once __DIR__ . '/MemUsage.php';

class File
{
    private const CHUNK = 100000;

    use \MemUsage;

    /** @var string путь к файлу */
    private $path;

    /** @var string имя файла */
    private $name;

    /** @var int|null количество строк в файле */
    private $totalLines = 0;

    private $handle = false;

    /** @var \SplFixedArray  */
    private $indexMap;

    private $count = 0;

    private $startTime;

    /**
     * File constructor.
     *
     * @param string $filePath
     * @param string $fileName
     *
     * @throws Exception
     */
    public function __construct(string $filePath, string $fileName)
    {
        $this->startTime = time();

        $this->path = $filePath;
        $this->name = $fileName;

        if (is_file($this->getPathAndName())) {
            $this->setTotalNumberLinesInFile();
            $this->indexMap = new \SplFixedArray($this->totalLines+1);
            $this->handle = fopen($this->getPathAndName(), 'rb');
        }

        if (!is_resource($this->handle)) {
            throw new \Exception(
                'Отсутствует указатель на файл ' . $this->getPathAndName()
            );
        }

        $this->definitionStrings();
    }

    /**
     * @return bool|resource
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * @return int|null
     */
    public function getTotalLines(): ?int
    {
        return $this->totalLines;
    }

    /**
     * @return string
     */
    public function getPathAndName(): string
    {
        return $this->path . DIRECTORY_SEPARATOR . $this->name;
    }

    /**
     * @throws Exception
     */
    private function definitionStrings()
    {
        $procBack = 0;
        $oneProc = $this->totalLines * 0.01;
        while (fgets($this->handle) !== false) {
            $this->indexMap[$this->count] = ftell($this->handle);
            $proc = round($this->count / $oneProc);
            if ($procBack < $proc) {
                $procBack = $proc;
                echo sprintf('%d%% ', $proc);
            }
            ++$this->count;
        }
    }

    /**
     * @param int $index
     *
     * @return string
     * @throws Exception
     */
    public function getString(int $index)
    {
        if ($index >= $this->totalLines) {
            throw new \Exception('Not');
        }
        if ($index === 0){
            fseek($this->handle, 0);
        } else {
            fseek($this->handle, $this->indexMap[$index - 1]);
        }

        return rtrim(fgets($this->handle), "\n");
    }

    /**
     * @return File
     */
    private function setTotalNumberLinesInFile(): self
    {
        $filePathAndName = $this->getPathAndName();
        // количество строк в файле
        exec('wc -l ' . $filePathAndName, $output, $exitCode);
        if ($exitCode === 0 && !empty($output[0])) {
            $lines = explode(' ', $output[0]);
            if (!empty($lines[0]) && is_numeric($lines[0])) {
                $this->totalLines = (int) $lines[0];
            }
        }

        return $this;
    }
}
