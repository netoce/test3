<?php
ini_set('memory_limit', '400M');

require_once __DIR__ . '/File.php';
require_once __DIR__ . '/FileIterator.php';
require_once __DIR__ . '/MemUsage.php';

// файл для чтения
const FNAME = 'text.txt';

$startTime = time();

// класс, реализующий интерфейс SeekableIterator
$iterator = new \FileIterator(
    new \File(__DIR__, FNAME)
);

// выборка 10000 случайных записей
for ($i=1; $i<=10000; $i++) {
    $position = mt_rand(0, ($iterator->getFile()->getTotalLines()+1));
    $iterator->seek($position);
    $result = $iterator->current();
    echo sprintf('%d) [%d] %s', $i, $position, $result) . PHP_EOL;
}

echo PHP_EOL
    . '=================' . PHP_EOL
    . File::viewMemUsage($startTime) . PHP_EOL
    . '=================' . PHP_EOL;
