<?php
class FileIterator implements \SeekableIterator
{
    /** @var int  */
    private $position = 0;

    /** @var File  */
    private $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    /**
     * Устанавливает смещение в указателе
     * @param int $position
     */
    public function seek($position)
    {
        if ($position < 0 || $position >= $this->file->getTotalLines()) {
            throw new \OutOfBoundsException("Alarm position $position !");
        }
        $this->position = $position;
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function current()
    {
        return $this->file->getString($this->position);
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->position < $this->file->getTotalLines();
    }

    /**
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }
}
